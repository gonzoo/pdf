<?php

namespace App\ExporterModel;

use App\ExporterModel\BankKiegyenlites;
use Money\Currency;
use Money\Money;

/**
 * Jimmy Bogard - Crafting Wicked Domain Models alapján készült
 */
class BankSor
{
    /**
     * @var ?string
     */
    protected $egyediAzonosito;

    /**
     * @var ?string
     */
    protected $tranzakcioszam;

    /**
     * @var \DateTime
     */
    protected $konyvelesDatuma;

    /**
     * @var \DateTime
     */
    protected $erteknap;

    /**
     * @var ?string
     *
     * A tranzakció ellenpartnere.
     * NEM a kiegyenlített számla partnere,
     * hanem az a partner, ami a kivonaton szerepel,
     * úgy írva, ahogy a kivonaton szerepel.
     *
     * A kiegyenlített számla partnere
     * a BankKiegyenlites.partnerNev mezőben van tárolva.
     */
    protected $partnerNev;

    /**
     * @var Money
     */
    protected $osszeg;

    /**
     * @var Currency
     *
     * Bankszámla devizaneme.
     */
    protected $deviza;

    /**
     * @var Money|null
     *
     * Akkor null, ha egyezik az $osszeg-gel.
     * Ha ez null, akkor az $ellenDeviza is null kell legyen.
     */
    protected $ellenOsszeg;

    /**
     * @var Currency|null
     *
     * Utalás devizaneme.
     * Akkor null, ha egyezik a (bankszámla) $deviza-val.
     * Ha ez null, akkor az $ellenOsszeg is null kell legyen.
     */
    protected $ellenDeviza;

    /**
     * @var Money|null
     *
     * Ha nem tudjuk kiszámolni, mert nincs hozzá adat,
     * akkor üresen marad.
     */
    protected $konyvelesOsszeg;

    /**
     * @var Currency
     *
     * Könyvelés alapdevizaneme.
     */
    protected $konyvelesDeviza;

    /**
     * @var Money|null
     *
     * Ha nem tudjuk kiszámolni, mert nincs hozzá adat,
     * akkor üresen marad.
     */
    protected $hufOsszeg;

    /**
     * @var string
     */
    protected $bankiFksz;

    /**
     * @var string
     */
    protected $ellenFksz;

    /**
     * @var string
     */
    protected $ellenFkszJellege;

    /**
     * @var string
     */
    protected $tartozikFksz;

    /**
     * @var string
     */
    protected $kovetelFksz;

    /**
     * @var ?string
     */
    protected $megjegyzes;

    /**
     * @var ?string
     */
    protected $munkaszam;

    /**
     * @var BankKiegyenlites
     */
    protected $bankKiegyenlites;

    // Innentől ezek a getterek a $bankKiegyenlites
    // adatait érik el. A __construct gondoskodik róla,
    // hogy legyen mindenképpen $bankKiegyenlites.
    // Annyi lehet, hogy üres string van minden mezőjében.

    /**
     * @return ?string
     */
    public function getBKPartnerKod()
    {
        return $this->bankKiegyenlites->getPartnerKod();
    }

    /**
     * @return ?string
     */
    public function getBKPartnerAdoszam()
    {
        return $this->bankKiegyenlites->getPartnerAdoszam();
    }

    /**
     * @return ?string
     */
    public function getBKPartnerKozossegiAdoszam()
    {
        return $this->bankKiegyenlites->getPartnerKozossegiAdoszam();
    }

    /**
     * @return ?string
     */
    public function getBKPartnerNev()
    {
        return $this->bankKiegyenlites->getPartnerNev();
    }

    /**
     * @return ?string
     */
    public function getBKPartnerPenzfogralmiAfas()
    {
        return $this->bankKiegyenlites->getPartnerPenzfogralmiAfas();
    }

    /**
     * @return ?string
     */
    public function getBKSzamlaNaploKod()
    {
        return $this->bankKiegyenlites->getSzamlaNaploKod();
    }

    /**
     * @return ?string
     */
    public function getBKSzamlaEgyediAzonosito()
    {
        return $this->bankKiegyenlites->getSzamlaEgyediAzonosito();
    }

    /**
     * @return ?string
     */
    public function getBKSzamlaSorszam()
    {
        return $this->bankKiegyenlites->getSzamlaSorszam();
    }

    /**
     * @return ?string
     */
    public function getBKSzamlaIranya()
    {
        return $this->bankKiegyenlites->getSzamlaIranya();
    }

    public function __construct(
        ?string $egyediAzonosito,
        ?string $tranzakcioszam,
        \DateTime $konyvelesDatuma,
        \DateTime $erteknap,
        ?string $partnerNev,

        Money $osszeg,
        Currency $deviza,

        ?Money $ellenOsszeg,
        ?Currency $ellenDeviza,
        ?Money $konyvelesOsszeg,

        Currency $konyvelesDeviza,
        ?Money $hufOsszeg,
        string $bankiFksz,
        string $ellenFksz,
        string $ellenFkszJellege,
        string $tartozikFksz,
        string $kovetelFksz,
        ?string $megjegyzes,
        ?string $munkaszam,
        BankKiegyenlites $bankKiegyenlites
    ) {
        $this->egyediAzonosito = $egyediAzonosito;
        $this->tranzakcioszam = $tranzakcioszam;
        $this->konyvelesDatuma = $konyvelesDatuma;
        $this->erteknap = $erteknap;
        $this->partnerNev = $partnerNev;
        $this->osszeg = $osszeg;
        $this->deviza = $deviza;
        $this->ellenOsszeg = $ellenOsszeg;
        $this->ellenDeviza = $ellenDeviza;
        $this->konyvelesOsszeg = $konyvelesOsszeg;
        $this->konyvelesDeviza = $konyvelesDeviza;
        $this->hufOsszeg = $hufOsszeg;
        $this->bankiFksz = $bankiFksz;
        $this->ellenFksz = $ellenFksz;
        $this->ellenFkszJellege = $ellenFkszJellege;
        $this->tartozikFksz = $tartozikFksz;
        $this->kovetelFksz = $kovetelFksz;
        $this->megjegyzes = $megjegyzes;
        $this->munkaszam = $munkaszam;
        $this->bankKiegyenlites = $bankKiegyenlites;
    }

    // Only getters are down from here.

    /**
     * @return ?string
     */
    public function getEgyediAzonosito()
    {
        return $this->egyediAzonosito;
    }

    /**
     * @return ?string
     */
    public function getTranzakcioszam()
    {
        return $this->tranzakcioszam;
    }

    /**
     * @return \DateTime
     */
    public function getKonyvelesDatuma()
    {
        return $this->konyvelesDatuma;
    }

    /**
     * @return \DateTime
     */
    public function getErteknap()
    {
        return $this->erteknap;
    }

    /**
     * @return ?string
     */
    public function getPartnerNev()
    {
        return $this->partnerNev;
    }

    /**
     * @return Money
     */
    public function getOsszeg()
    {
        return $this->osszeg;
    }

    /**
     * @return Currency
     */
    public function getDeviza()
    {
        return $this->deviza;
    }

    /**
     * @return Money
     */
    public function getEllenOsszeg()
    {
        return $this->ellenOsszeg;
    }

    /**
     * @return Currency
     */
    public function getEllenDeviza()
    {
        return $this->ellenDeviza;
    }

    /**
     * @return Money
     */
    public function getKonyvelesOsszeg()
    {
        return $this->konyvelesOsszeg;
    }

    /**
     * @return Currency
     */
    public function getKonyvelesDeviza()
    {
        return $this->konyvelesDeviza;
    }

    /**
     * @return Money
     */
    public function getHufOsszeg()
    {
        return $this->hufOsszeg;
    }

    /**
     * @return string
     */
    public function getBankiFksz()
    {
        return $this->bankiFksz;
    }

    /**
     * @return string
     */
    public function getEllenFksz()
    {
        return $this->ellenFksz;
    }

    /**
     * @return string
     */
    public function getEllenFkszJellege()
    {
        return $this->ellenFkszJellege;
    }

    /**
     * @return string
     */
    public function getTartozikFksz()
    {
        return $this->tartozikFksz;
    }

    /**
     * @return string
     */
    public function getKovetelFksz()
    {
        return $this->kovetelFksz;
    }

    /**
     * @return ?string
     */
    public function getMegjegyzes()
    {
        return $this->megjegyzes;
    }

    /**
     * @return ?string
     */
    public function getMunkaszam()
    {
        return $this->munkaszam;
    }

    /**
     * @return BankKiegyenlites
     */
    public function getBankKiegyenlites()
    {
        return $this->bankKiegyenlites;
    }
}
