<?php

namespace App\ExporterModel;

/**
 * Jimmy Bogard - Crafting Wicked Domain Models alapján készült
 */
class BankKiegyenlites
{
    /**
     * @var ?string
     */
    protected $partnerKod;

    /**
     * @var ?string
     */
    protected $partnerAdoszam;

    /**
     * @var ?string
     */
    protected $partnerKozossegiAdoszam;

    /**
     * @var ?string
     */
    protected $partnerNev;

    /**
     * @var ?string
     */
    protected $partnerPenzfogralmiAfas;

    /**
     * @var ?string
     */
    protected $szamlaNaploKod;

    /**
     * @var ?string
     */
    protected $szamlaEgyediAzonosito;

    /**
     * @var ?string
     */
    protected $szamlaSorszam;

    /**
     * @var ?string
     *
     * V: vevő, S: szállító
     */
    protected $szamlaIranya;

    public function __construct(
        ?string $partnerKod,
        ?string $partnerAdoszam,
        ?string $partnerKozossegiAdoszam,
        ?string $partnerNev,
        ?string $partnerPenzfogralmiAfas,
        ?string $szamlaNaploKod,
        ?string $szamlaEgyediAzonosito,
        ?string $szamlaSorszam,
        ?string $szamlaIranya
    ) {
        $this->partnerKod = $partnerKod;
        $this->partnerAdoszam = $partnerAdoszam;
        $this->partnerKozossegiAdoszam = $partnerKozossegiAdoszam;
        $this->partnerNev = $partnerNev;
        $this->partnerPenzfogralmiAfas = $partnerPenzfogralmiAfas;
        $this->szamlaNaploKod = $szamlaNaploKod;
        $this->szamlaEgyediAzonosito = $szamlaEgyediAzonosito;
        $this->szamlaSorszam = $szamlaSorszam;
        $this->szamlaIranya = $szamlaIranya;
    }

    // Only getters are down from here.

    /**
     * @return ?string
     */
    public function getPartnerKod()
    {
        return $this->partnerKod;
    }

    /**
     * @return ?string
     */
    public function getPartnerAdoszam()
    {
        return $this->partnerAdoszam;
    }

    /**
     * @return ?string
     */
    public function getPartnerKozossegiAdoszam()
    {
        return $this->partnerKozossegiAdoszam;
    }

    /**
     * @return ?string
     */
    public function getPartnerNev()
    {
        return $this->partnerNev;
    }

    /**
     * @return ?string
     */
    public function getPartnerPenzfogralmiAfas()
    {
        return $this->partnerPenzfogralmiAfas;
    }

    /**
     * @return ?string
     */
    public function getSzamlaNaploKod()
    {
        return $this->szamlaNaploKod;
    }

    /**
     * @return ?string
     */
    public function getSzamlaEgyediAzonosito()
    {
        return $this->szamlaEgyediAzonosito;
    }

    /**
     * @return ?string
     */
    public function getSzamlaSorszam()
    {
        return $this->szamlaSorszam;
    }

    /**
     * @return ?string
     */
    public function getSzamlaIranya()
    {
        return $this->szamlaIranya;
    }
}
