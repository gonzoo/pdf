<?php

namespace App\Controller;

use App\ExporterModel\BankKiegyenlites;
use App\ExporterModel\BankSor;
use App\Service\PDFService;
use Doctrine\Common\Collections\ArrayCollection;
use Faker\Factory;
use Money\Currency;
use Money\Money;
use Mpdf\Mpdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="index", defaults={"view"= false})
     * @Route("/view", name="index_pdf", defaults={"view"= true})
     */
    public function index($view)
    {

        $importRows = new ArrayCollection();

        $faker = Factory::create();

        $vat = 'xxxxxxxx-y-zz';
        for ($i = 0; $i < 30; $i++) {
            $bankSor = new BankSor(
                $i,
                $faker->randomDigit,
                $faker->dateTime,
                $faker->dateTime,
                $faker->name('male'),

                new Money($faker->numberBetween(1111, 9999999), new Currency($faker->currencyCode)),
                new Currency($faker->currencyCode),

                new Money($faker->numberBetween(1111, 9999999), new Currency($faker->currencyCode)),
                new Currency($faker->currencyCode),
                new Money($faker->numberBetween(1111, 9999999), new Currency($faker->currencyCode)),

                new Currency($faker->currencyCode),
                new Money($faker->numberBetween(1111, 9999999), new Currency($faker->currencyCode)),

                $faker->word,
                $faker->word,
                $faker->word,
                $faker->word,
                $faker->word,
                false,
                false,
                new BankKiegyenlites(
                    $vat,
                    $vat,
                    $vat,
                    $vat,
                    $vat,
                    $vat,
                    $vat,
                    $faker->boolean ? '' : '',
                    'V'
                )
            );

            $importRows->add($bankSor);
        }

        $bankSorokPdfre = [];

        foreach ($importRows as $bankSor) {
            $szamlaSorszam = $bankSor->getBKSzamlaSorszam();
            if ($szamlaSorszam) {
                // Ha a $szamlaSorszam be van írva, akkor van számla egyezés.

                // Itt a kód beteszi egy json-be, de ez a PDF szempontjából mindegy.
                // ...
            } else {
                // Ha a $szamlaSorszam nincs beírva, akkor nem talált egyezést.

                // Itt gyűjtjük ki azokat a banki sorokat, amik rákerülnek a PDF-re.
                $bankSorokPdfre[] = $bankSor;
            }
        }


        if (!empty($bankSorokPdfre)) {

            $html = $this->renderView('pdf/table.html.twig', [
                'rows' => $bankSorokPdfre,
            ]);

            if ($view) {
                return (new PDFService())
                    ->setHTML($html)
                    ->setProtection()
                    ->getPDF();
            } else {
                $pdfImage = (new PDFService())
                    ->setHTML($html);

                $im = new \Imagick();
                $im->setResolution(200, 200);
                $im->readImageBlob(($pdfImage->getPDF('', 'S')));

                $finalPDF = (new PDFService())
                    ->addHeaderFooter();

                for ($pageNum = 0; $pageNum < $im->getNumberImages(); $pageNum++) {
                    $im->setIteratorIndex($pageNum);
                    $im->setImageFormat('png');
                    $finalPDF->AddPage('<div style="text-align: center"><img src="data:image/png;base64,' . base64_encode($im->getImageBlob()) . '"/></div>');
                }

                $finalPDF->getPDF('', 'I');
            }
        }

        dump('Nincs adat a PDF generalashoz');
        exit;

    }
}
